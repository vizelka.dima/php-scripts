<?php

namespace HW\Tests;

use HW\Lib\LinkedList;
use HW\Lib\LinkedListItem;
use PHPUnit\Framework\TestCase;


class LinkedListItemTest extends TestCase
{
    protected $list;

    public function setUp(): void
    {
        parent::setUp();
        $this->list = new LinkedList();
    }

    /*---------------LIST_ITEM---------------------------*/
    public function testCreateListItem()
    {
        $item = new LinkedListItem("testItem");
        $this->assertInstanceOf(LinkedListItem::class,$item);
    }

    public function testGetSetValue()
    {
        $item = new LinkedListItem("testItem");
        $this->assertEquals("testItem",$item->getValue());

        $item->setValue("setedItem");
        $this->assertEquals("setedItem",$item->getValue());
    }

    public function testGetSetNext()
    {
        $item = new LinkedListItem("testItem");
        $next = new LinkedListItem("testNextItem");
        $item->setNext($next);
        self::assertInstanceOf(LinkedListItem::class,$item->getNext());
        self::assertEquals($next,$item->getNext());
    }

    public function testGetSetPrev()
    {
        $item = new LinkedListItem("testItem");
        $prev = new LinkedListItem("testPrevItem");
        $item->setPrev($prev);
        self::assertInstanceOf(LinkedListItem::class,$item->getPrev());
        self::assertEquals($prev,$item->getPrev());
    }

    /*--------------------LIST-----------------------------*/
    public function testEmptyList()
    {
        self::assertNull($this->list->getFirst());
        self::assertNull($this->list->getLast());
    }


    /*-------------------Helpers--------------------------*/
    public function bothDirections($len)
    {
        $this->leftToRight($len);
        $this->rightToLeft($len);
    }

    public function leftToRight($len)
    {
        $item = $this->list->getFirst();
        for ($i=1;$i<=$len;++$i)
        {
            self::assertNotNull($item);
            self::assertInstanceOf(LinkedListItem::class,$item);
            self::assertEquals("test".$i, $item->getValue());
            $item = $item->getNext();
        }
        self::assertNull($item);
    }

    public function rightToLeft($len)
    {
        $item = $this->list->getLast();
        for ($i=$len;$i>=1;--$i)
        {
            self::assertNotNull($item);
            self::assertInstanceOf(LinkedListItem::class,$item);
            self::assertEquals("test".$i, $item->getValue());
            $item = $item->getPrev();
        }
        self::assertNull($item);
    }

    /*-------------------PrependList-------------------*/
    public function testPrependListOne()
    {
        $this->list->prependList("test1");
        $this->bothDirections(1);
    }

    public function testPrependListTwo()
    {
        $this->list->prependList("test2");
        $this->list->prependList("test1");
        $this->bothDirections(2);
    }

    public function testPrependListThree()
    {
        $this->list->prependList("test3");
        $this->list->prependList("test2");
        $this->list->prependList("test1");
        $this->bothDirections(3);
    }

    public function testPrependListFour()
    {
        $this->list->prependList("test4");
        $this->list->prependList("test3");
        $this->list->prependList("test2");
        $this->list->prependList("test1");
        $this->bothDirections(4);
    }

    /*-------------------AppendList-------------------*/
    public function testAppendListOne()
    {
        $this->list->appendList("test1");
        $this->bothDirections(1);
    }

    public function testAppendListTwo()
    {
        $this->list->appendList("test1");
        $this->list->appendList("test2");
        $this->bothDirections(2);
    }

    public function testAppendListThree()
    {
        $this->list->appendList("test1");
        $this->list->appendList("test2");
        $this->list->appendList("test3");
        $this->bothDirections(3);
    }

    public function testAppendListFour()
    {
        $this->list->appendList("test1");
        $this->list->appendList("test2");
        $this->list->appendList("test3");
        $this->list->appendList("test4");
        $this->bothDirections(4);
    }

    /*-------------------PrependListItem-------------------*/
    public function testPrependItemNotInEmptyList()
    {
        //Prepend item that is not in list
        self::expectException(\InvalidArgumentException::class);
        $this->list->prependItem(new LinkedListItem("test1"), "test2");
    }

    public function testPrependItemNotInList()
    {
        //Prepend item that is not in list
        $this->list->prependList("test3");
        $this->list->prependList("test2");
        $this->list->prependList("test1");

        self::expectException(\InvalidArgumentException::class);
        $this->list->prependItem(new LinkedListItem("test8"), "test4");
    }

    public function testPrependItemIsInListOne()
    {
        $second = $this->list->prependList("test2");
        $this->list->prependItem($second,"test1");
        $this->bothDirections(2);
    }

    public function testPrependItemFirst()
    {
        $first = $this->list->appendList("test2");
        $this->list->appendList("test3");

        $this->list->prependItem($first,"test1");
        $this->bothDirections(3);
    }

    public function testPrependItemLast()
    {
        $this->list->appendList("test1");
        $last = $this->list->appendList("test3");

        $this->list->prependItem($last,"test2");
        $this->bothDirections(3);
    }

    public function testPrependItemMiddle()
    {
        $this->list->appendList("test1");
        $middle = $this->list->appendList("test3");
        $this->list->appendList("test4");

        $this->list->prependItem($middle,"test2");
        $this->bothDirections(4);
    }

    /*-------------------AppendListItem-------------------*/
    public function testAppendItemNotInEmptyList()
    {
        //Append item that is not in list
        self::expectException(\InvalidArgumentException::class);
        $this->list->appendItem(new LinkedListItem("test2"), "test1");
    }

    public function testAppendItemNotInList()
    {
        //Append item that is not in list
        $this->list->prependList("test3");
        $this->list->prependList("test2");
        $this->list->prependList("test1");

        self::expectException(\InvalidArgumentException::class);
        $this->list->appendItem(new LinkedListItem("test8"), "test0");
    }

    public function testAppendItemIsInListOne()
    {
        $first = $this->list->prependList("test1");
        $this->list->appendItem($first,"test2");
        $this->bothDirections(2);
    }

    public function testAppendItemFirst()
    {
        $first = $this->list->appendList("test1");
        $this->list->appendList("test3");

        $this->list->appendItem($first,"test2");
        $this->bothDirections(3);
    }

    public function testAppendItemLast()
    {
        $this->list->appendList("test1");
        $last = $this->list->appendList("test2");

        $this->list->appendItem($last,"test3");
        $this->bothDirections(3);
    }

    public function testAppendItemMiddle()
    {
        $this->list->appendList("test1");
        $middle = $this->list->appendList("test2");
        $this->list->appendList("test4");

        $this->list->appendItem($middle,"test3");
        $this->bothDirections(4);
    }
}