<?php

function getPrice($item) {
    $pattern = "/((?<=CZK)\s*(\d+\.?)+\,?\d*)|((\d+\.?)+\,?\d*\s*(?=Kč|CZK|,-))/u";
    $found = preg_match($pattern, $item, $match);

    $price = 0;
    if($found == true) {
        $price = floatval(str_replace(['.', ','], ['', '.'], $match[0]));
    }

    return $price;
}

function sortList($list) {
    usort($list,fn($a, $b) => getPrice($a) <= getprice($b) ? -1 : 1);
    return $list;
}

function sumList($list) {
    return array_reduce($list,fn($sum, $p) => $sum+getPrice($p),0);
}

if (count($argv) !== 2) {
	echo "Usage: php shopping.php <input>\n";
	exit(1);
}

$input = file_get_contents(end($argv));
$list = explode(PHP_EOL, $input);
$list = sortList($list);
print_r($list);
print_r(sumList($list) . PHP_EOL);