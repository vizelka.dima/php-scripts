<?php

namespace Iterator;

use Node;

class PreOrderIterator extends AbstractOrderIterator
{
    public function saveOrdered(?Node $root)
    {
        if($root==null)
            return;
        $this->nodes[] = $root;
        $this->saveOrdered($root->getLeft());
        $this->saveOrdered($root->getRight());
    }
}
