<?php

namespace Iterator;

use Node;

class InOrderIterator extends AbstractOrderIterator
{
    public function saveOrdered(?Node $root)
    {
        if($root==null)
            return;
        $this->saveOrdered($root->getLeft());
        $this->nodes[] = $root;
        $this->saveOrdered($root->getRight());
    }
}
