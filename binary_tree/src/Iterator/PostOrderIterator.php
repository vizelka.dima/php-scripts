<?php

namespace Iterator;

use Node;

class PostOrderIterator extends AbstractOrderIterator
{
    public function saveOrdered(?Node $root)
    {
        if($root==null)
            return;
        $this->saveOrdered($root->getLeft());
        $this->saveOrdered($root->getRight());
        $this->nodes[] = $root;
    }
}
