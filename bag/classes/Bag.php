<?php

class Bag {
    private array $col = [];

    public function add($item) {
        $this->col[] = $item;
    }

    public function clear() {
        $this->col = [];
    }

    public function contains($item) : bool{
        return in_array($item, $this->col);
    }

    public function elementSize($item){
        $freq = 0;
        foreach ($this->col as $citem) {
            if($item === $citem) ++$freq;
        }
        return $freq;
    }

    public function isEmpty() : bool{
        return empty($this->col);
    }

    public function remove($item) {
        $key = array_search($item,$this->col);
        if ($key>=0){
            unset($this->col[$key]);
        }
    }

    public function size() : int{
        return count($this->col);
    }
}