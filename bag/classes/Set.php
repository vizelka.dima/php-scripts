<?php


class Set extends Bag
{
    public function add($item) {
        if (!($this->contains($item))) {
            parent::add($item);
        }
    }
}