<?php

const DEAD = '.';
const ALIVE = 'X';

function readInput($string){
    return explode("\n", $string);
}

function writeOutput($matrix) {
    return implode( "\n",$matrix);
}

function gameStep($matrix) {
    $next = $matrix;
    $y = 0;

    $len = empty($matrix) ? 0 : strlen($matrix[0]) - 1;

    foreach ($matrix as $line) {

        for($x = 0; $x < $len; $x++) {
            $alive_cnt = 0;
            for($d = -1; $d <= 1; $d++)
            {
                for($t = -1; $t <= 1; $t++)
                {
                    if($d == 0 && $t == 0){continue;}
                    if( array_key_exists($y+$d, $matrix) &&
                        $x+$t < $len &&
                        $matrix[$y+$d][$x+$t] == ALIVE )
                    { ++$alive_cnt; }
                }
            }

            if($line[$x] == ALIVE && ($alive_cnt < 2 || $alive_cnt > 3))
            { $next[$y][$x]=DEAD; }

            else if($line[$x]==DEAD && $alive_cnt == 3)
            { $next[$y][$x]=ALIVE; }
        }
        ++$y;
    }
    return $next;
}
