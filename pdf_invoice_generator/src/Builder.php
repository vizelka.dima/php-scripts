<?php declare(strict_types=1);


namespace App;

use App\Invoice\Address;
use App\Invoice\BusinessEntity;
use App\Invoice\Item;

class Builder
{
    /** @var Invoice */
    protected $invoice;

    /**
     * Builder constructor.
     * @param Invoice $invoice
     */
    public function __construct()
    {
        $this->invoice = new Invoice();
    }

    /**
     * @return Invoice
     */
    public function build(): Invoice
    {
        return $this->invoice;
    }

    /**
     * @param string $number
     * @return $this
     */
    public function setNumber(string $number): self
    {
        $this->invoice->setNumber($number);
        return $this;
    }


    /**
     * @param string      $name
     * @param string      $vatNumber
     * @param string      $street
     * @param string      $number
     * @param string      $city
     * @param string      $zip
     * @param string|null $phone
     * @param string|null $email
     * @return $this
     */
    public function setSupplier(
        string $name,
        string $vatNumber,
        string $street,
        string $number,
        string $city,
        string $zip,
        ?string $phone = null,
        ?string $email = null
    ): self {
        $add = new Address();
        $add -> setStreet($street);
        $add -> setNumber($number);
        $add -> setCity($city);
        $add -> setZipCode($zip);
        $add -> setPhone($phone);
        $add -> setEmail($email);

        $sup = new BusinessEntity();
        $sup -> setName($name);
        $sup -> setVatNumber($vatNumber);

        $sup -> setAddress($add);

        $this->invoice->setSupplier($sup);
        return $this;
    }

    /**
     * @param string      $name
     * @param string      $vatNumber
     * @param string      $street
     * @param string      $number
     * @param string      $city
     * @param string      $zip
     * @param string|null $phone
     * @param string|null $email
     * @return $this
     */
    public function setCustomer(
        string $name,
        string $vatNumber,
        string $street,
        string $number,
        string $city,
        string $zip,
        ?string $phone = null,
        ?string $email = null
    ): self {
        $add = new Address();
        $add -> setStreet($street);
        $add -> setNumber($number);
        $add -> setCity($city);
        $add -> setZipCode($zip);
        $add -> setPhone($phone);
        $add -> setEmail($email);

        $sup = new BusinessEntity();
        $sup -> setName($name);
        $sup -> setVatNumber($vatNumber);

        $sup -> setAddress($add);

        $this->invoice->setCustomer($sup);
        return $this;
    }

    /**
     * @param string     $description
     * @param float|null $quantity
     * @param float|null $price
     * @return $this
     */
    public function addItem(string $description, ?float $quantity, ?float $price): self
    {
        $item = new Item();
        $item->setDescription($description);
        $item->setQuantity($quantity);
        $item->setUnitPrice($price);

        $this->invoice->addItem($item);
        return $this;
    }
}
